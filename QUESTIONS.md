## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?

R. HTML5 E CSS3 com farinha e pimenta e o Book CakePHP.

2. Quais foram os últimos dois framework/CMS que você trabalhou?

R. CakePHP e Jquery Mobile.

3. Descreva os principais pontos positivos do seu framework favorito:

R. CakePHP: Por trabalhar com aplicações pequenas e de médio porte, escolhi o cake devido a sua curva de aprendizagem menor. E por ser
um framework bastante utilizado, com uma comunidade participativa. Como vários outros ele faz uso da arquitetura MVC (Model View Controller), facilitando
o desenvolvimento do projeto. E posso citar ainda a documentação excelente disponível e é bastante robusto.

4. Descreva os principais pontos negativos do seu framework favorito:

R. Por ser um framework de desenvolvimento rápido, ele acabe perdendo performance em aplicações maiores. Sendo assim, seria
interessente utilizar por exemplo o Zend.

5. O que é código de qualidade para você?

R. É um código limpo e expressivo, onde qualquer outro programar possa entender para
poder trabalhar nele sem ter problema algum.

## Conhecimento Linux

1. O que é software livre?

R. Liberdade ao programador rsrsrs. É qualquer programa que pode ser copiado, 
usado, modificado e redistribuído por qualquer pessoa.

2. Qual o seu sistema operacional favorito?

R. Apesar da maioria de meus programas serem livres, eu prefiro utilizar o windows. Mas
não será problema utilizar outro s.o

3. Já trabalhou com Linux ou outro s.o baseado em Unix?

R. Trabalhei com o Linux por um curto período no início da faculdade.

4. O que é SSH?

R. É um protocolo e também um software que podemos conectar com outro com computador
em uma rende, podendo efetura comandos através de uma máquina remota.

5. Quais as principais diferenças entre sistemas baseados no Unix e o Windows?

R. Pra mim, a maior diferença é a distribuição livre do Unix, este que é o pai dos SOs, que é o oposto do Windows.  Muitos se perguntam por que a Microsoft
não utiliza o Unix, e uma possível resposta pode ser a falta de interesse.

## Conhecimento de desenvolvimento

1. O que é GIT?

R. Sistema de controle de versão.

2. Descreva um workflow simples de trabalho utilizando GIT.

R. A medida que novas alterações vão sendo acrescidas ao repositório, novo conteúdo é gerado no repositório. Assim,
	dependendo da alteração que você se concentrar, um diferente tipo de conteúdo você pode ter.

3. O que é PHP Data Objects?

R. PDO é um módulo montado sob o paradigma OO. E prover uma patronização da forma que
o PHP se comunica com o banco de dados relacional.

4. O que é Database Abstract Layer?

R. É uma interface de programação que unifica a comunicação entre a aplicação
e as bases de dados.

5. Você sabe o que é Object Relational Mapping? Se sim, explique.

R. Infelizmente até o momento não sei. Mas já estou procurando aprender!ss

6. Como você avalia seu grau de conhecimento em Orientação a objeto?

R. Vejo que ainda tenho muito a aprender, mas posso trabalhar tranquilamente e as dúvidas que aparecem,
sempre procuro na internet exemplos e com isso vou sempre aprendendo. Como por exemplo a pergunta anterior. Na
próxima vez que fizerem essa pergunta já saberei responder.

7. O que é Dependency Injection?

R. Nunca utilizei, mas é um padrão de desenvolvimento de programas utilizado quando é necessário manter baixo o nível de aclopamento.

8. O significa a sigla S.O.L.I.D?

R. S.O.L.I.D – Coisas que todo programador deveria saber (Até o momento não sabia o que era, hehehe).
	É um acrônomo onde cada letra tem uma representação.
	
	S - Toda classe deve ter uma única responsabilidade.
	O - Todas as entidades (classes, módulos, métodos, ...) deveriam ser abertas para extensão e fechadas para modificação.

9. Qual a finalidade do framework PHPUnit?

R. Desenvolver testes unitários (pequenas partes) em PHP.

10. Explique o que é MVC.

R. Modelo de arquitura de software que é utilizado para separação da representação da informação da interação do usuário com ele.
	O Model consiste nos dados da aplicação. Uma view pode ser qualquer saída de representação de dados. E o controller faz a mediação entre
	o model e a view.