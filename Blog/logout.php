<?php

require_once './config.php';

use application\lib\AppSession;
use application\lib\AppSystem;

// Limpando a sess�o e redirecionando para tela de login
AppSession::freeSession();
AppSystem::_redirect("/index.php");
