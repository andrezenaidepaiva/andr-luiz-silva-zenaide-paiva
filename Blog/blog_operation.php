<?php
require_once './config.php';

use application\Blog\Blog;
use application\BlogCategoria\BlogCategoria;
use application\lib\AppSystem;

// Pegando id via 
$id = $_REQUEST["id"];
// Caso o id exista, preencher o objeto para popular os campos do formul�rio
$objBlog = new Blog();
if (!empty($id)) {
    $objBlog->setId($id);
    $objBlog->load();
}

// Listando registros da categoria para carregar no select
$objBlogCategoria = new BlogCategoria();
$list = $objBlogCategoria->listBlogCategoria();

// verifica se o formul�rio enviou dados via POST
if (!empty($_POST)) {
    // Setar os par�metros do objeto com os dados do formul�rio para que possam ser tratados simultaneamente
    $objBlog->setParam($_POST["id"], $_POST["id_categoria"], $_POST["title"], $_POST["slug"], $_POST["description"], $_POST["body"], $_POST["author"], $_POST["insert_date"], $_POST["update_date"]);
    // Executa uma opera��o no banco de dados INSERT/UPDATE
    $result = $objBlog->executeOperation($_POST);
    // Caso o retorno seja um array retornar uma mensagem de erro na tela
    // Do contr�rio concluir a opera��o e redirecionar para a p�gina principal do m�dulo
    if (is_array($result)) {
        AppSystem::_alertMensagem("Aten��o", $result);
    } else {
        AppSystem::_confirmRedirect("Dados cadastrados com sucesso", "/blog.php");
    }
}
?>
<?php include_once './header.php'; ?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Gerenciando Postagem</h1>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php
                    if (empty($id)) {
                        echo "Adicionar Novo";
                    } else {
                        echo "Alterar Registro";
                    }
                    ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" name="form" method="post">
                                <div class="form-group">
                                    <label>Categoria</label>
                                    <select name="id_categoria" class="form-control">
                                        <option value="">[Selecione uma Categoria]</option>
                                        <?php for ($i = 0; $i < count($list); $i++) { ?>
                                        <option value="<?php echo $list[$i]["id"]?>" <?php if ($list[$i]["id"] == $objBlog->getIdCategoria()) { echo "selected='selected'"; }?>><?php echo $list[$i]["title"]?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>T�tulo</label>
                                    <input name="title" class="form-control" value="<?php echo AppSystem::textByHtml($objBlog->getTitle()); ?>" placeholder="Campo T�tulo" />
                                    <input name="slug" type="hidden" value="<?php echo AppSystem::textByHtml($objBlog->getSlug()); ?>" />
                                </div>
                                <div class="form-group">
                                    <label>Descri��o</label>
                                    <textarea name="description" class="form-control" rows="10" maxlength="500" placeholder="Campo Descri��o"><?php echo AppSystem::textByHtml($objBlog->getDescription()); ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Texto</label>
                                    <textarea name="body" class="form-control" rows="20" placeholder="Campo Texto"><?php echo AppSystem::textByHtml($objBlog->getBody()); ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Autor do Post</label>
                                    <input name="author" class="form-control" value="<?php echo AppSystem::textByHtml($objBlog->getAuthor()); ?>" placeholder="Campo autor" />
                                </div>
                                <?php if (empty($id)) { ?>
                                <input name="insert_date" type="hidden" value="<?php echo $objBlog->getInsertDate(); ?>" />
                                <?php } else { ?>
                                <input name="update_date" type="hidden" value="<?php echo $objBlog->getUpdateDate(); ?>" />
                                <?php } ?>
                                <?php if(!empty($id)) { ?>
                                <input name="id" type="hidden" value="<?php echo $objBlog->getId(); ?>" />
                                <?php } ?>
                                <button type="submit" class="btn btn-default">Salvar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /#wrapper -->
<?php include_once './footer.php'; ?>