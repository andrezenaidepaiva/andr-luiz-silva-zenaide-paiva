<?php
require_once './config.php';

use application\BlogCategoria\BlogCategoria;
use application\lib\AppSystem;

// Requisitando id para excluir registro
$id = $_REQUEST["id"];

$objBlogCategoria = new BlogCategoria();
// Setando vari�vel id no objeto
$objBlogCategoria->setId($id);
// Consultando Registro no objeto
$objBlogCategoria->load();

?>
<?php include_once './header.php'; ?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Visualizar Categoria</h1>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Visualizar Registro
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" name="form" method="post">
                                <div class="form-group">
                                    <label>C�digo: <?php echo $objBlogCategoria->getId(); ?></label>
                                </div>
                                <div class="form-group">
                                    <label>T�tulo: <?php echo $objBlogCategoria->getTitle(); ?></label>
                                </div>
                                <div class="form-group">
                                    <label>Slug: <?php echo AppSystem::textByHtml($objBlogCategoria->getSlug()); ?></label>
                                </div>
                                <a href="/blog_categoria.php" class="btn btn-default">Retornar</a>
                            </form>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /#wrapper -->
<?php include_once './footer.php'; ?>