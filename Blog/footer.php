<!-- Core Scripts - Include with every page -->
<script src="scripts/sb-admin-v2/js/jquery-1.10.2.js"></script>
<script src="scripts/sb-admin-v2/js/bootstrap.min.js"></script>
<script src="scripts/sb-admin-v2/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Page-Level Plugin Scripts - Blank -->

<!-- SB Admin Scripts - Include with every page -->
<script src="scripts/sb-admin-v2/js/sb-admin.js"></script>

<!-- Page-Level Demo Scripts - Blank - Use for reference -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#Msg').modal('show');
    });
</script>
</body>

</html>
