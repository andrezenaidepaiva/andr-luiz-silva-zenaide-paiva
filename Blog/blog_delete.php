<?php
require_once './config.php';

use application\Blog\Blog;
use application\lib\AppSystem;

// Requisitando id para excluir registro
$id = $_REQUEST["id"];

$objBlog = new Blog();
// Removendo Registro
$objBlog->removeRegistro("blog", "id", $id);
// Redirecionando para Listagem
AppSystem::_redirect("/user.php");

?>
