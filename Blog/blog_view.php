<?php
require_once './config.php';

use application\Blog\Blog;
use application\lib\AppSystem;

// Requisitando id para excluir registro
$id = $_REQUEST["id"];

$objBlog = new Blog();
// Setando vari�vel id no objeto
$objBlog->setId($id);
// Consultando Registro no objeto
$objBlog->load();
?>
<?php include_once './header.php'; ?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Visualizar Postagem</h1>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Visualizar Registro
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" name="form" method="post">
                                <div class="form-group">
                                    <label>C�digo: <?php echo $objBlog->getId(); ?></label>
                                </div>
                                <div class="form-group">
                                    <label>Categoria: <?php echo $objBlog->Categoria; ?></label>
                                </div>
                                <div class="form-group">
                                    <label>T�tulo: <?php echo AppSystem::textByHtml($objBlog->getTitle()); ?></label>
                                </div>
                                <div class="form-group">
                                    <label>Slug: <?php echo $objBlog->getSlug(); ?></label>
                                </div>
                                <div class="form-group">
                                    <label>Descri��o: <?php echo AppSystem::textByHtml($objBlog->getDescription()); ?></label>
                                </div>
                                <div class="form-group">
                                    <label>Texto: <?php echo AppSystem::textByHtml($objBlog->getBody()); ?></label>
                                </div>
                                <div class="form-group">
                                    <label>Autor: <?php echo $objBlog->getAuthor(); ?></label>
                                </div>
                                <div class="form-group">
                                    <label>Data de Inser��o: <?php echo AppSystem::formatarData($objBlog->getInsertDate(), "d/m/Y H:i:s"); ?> ?></label>
                                </div>
                                <div class="form-group">
                                    <label>Data de Autaliza��o: <?php echo AppSystem::formatarData($objBlog->getUpdateDate(), "d/m/Y H:i:s"); ?></label>
                                </div>
                                <a href="/blog.php" class="btn btn-default">Retornar</a>
                            </form>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /#wrapper -->
<?php include_once './footer.php'; ?>