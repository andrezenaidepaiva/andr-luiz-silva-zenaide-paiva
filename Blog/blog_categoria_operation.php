<?php
require_once './config.php';

use application\BlogCategoria\BlogCategoria;
use application\lib\AppSystem;

// Pegando id via 
$id = $_REQUEST["id"];
// Caso o id exista, preencher o objeto para popular os campos do formul�rio
$objBlogCategoria = new BlogCategoria();
if (!empty($id)) {
    $objBlogCategoria->setId($id);
    $objBlogCategoria->load();
}

// verifica se o formul�rio enviou dados via POST
if (!empty($_POST)) {
    // Setar os par�metros do objeto com os dados do formul�rio para que possam ser tratados simultaneamente
    $objBlogCategoria->setParam($_POST["id"], $_POST["title"], $_POST["slug"]);
    // Executa uma opera��o no banco de dados INSERT/UPDATE
    $result = $objBlogCategoria->executeOperation($_POST);
    // Caso o retorno seja um array retornar uma mensagem de erro na tela
    // Do contr�rio concluir a opera��o e redirecionar para a p�gina principal do m�dulo
    if (is_array($result)) {
        AppSystem::_alertMensagem("Aten��o", $result);
    } else {
        AppSystem::_confirmRedirect("Dados cadastrados com sucesso", "/blog_categoria.php");
    }
}
?>
<?php include_once './header.php'; ?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Gerenciando Categoria</h1>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php if (empty($id)) {echo "Adicionar Novo";} else {echo "Alterar Registro";} ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" name="form" method="post">
                                <div class="form-group">
                                    <label>T�tulo</label>
                                    <input name="title" class="form-control" value="<?php echo $objBlogCategoria->getTitle(); ?>" placeholder="Campo t�tulo" />
                                    <input name="slug" type="hidden" value="<?php echo AppSystem::textByHtml($objBlogCategoria->getSlug()); ?>" />
                                </div>
                                <?php if(!empty($id)) { ?>
                                <input name="id" type="hidden" value="<?php echo $objBlogCategoria->getId(); ?>" />
                                <?php } ?>
                                <button type="submit" class="btn btn-default">Salvar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /#wrapper -->
<?php include_once './footer.php'; ?>