<?php

namespace application\User;

use application\lib\AEntidade;
use application\lib\AppSystem;
use application\lib\AppDao;
use application\lib\AppSession;
use application\lib\AppInstructionSql;
use application\lib\AppDb;

/**
 * Description of Usuario
 * Classe responsavel pela entidade da Usu�rio
 * @author allan roberto
 */
class User extends AEntidade {

    private $userName;
    private $password;

    /**
     * M�todo getUserName
     * @return type $userName
     */
    public function getUserName() {
        return $this->userName;
    }

    /**
     * M�todo getPassword
     * @return type $password
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * M�todo setUserName
     * Trata o campo nome do usu�rio
     * @param type $userName
     */
    public function setUserName($userName) {
        $this->userName = AppSystem::htmlByText($userName);
    }

    /**
     * M�todo setPassword
     * Trata o campo senha usu�rio
     * @param type $password
     */
    public function setPassword($password) {
        $this->password = AppSystem::htmlByText($password);
    }

    /**
     * Metodo __toString
     * @return type attributo a ser retornado de forma padr�o pela classe
     */
    public function __toString() {
        return $this->getUserName();
    }

    /**
     * Metodo setParam
     * Responsavel por tratar todos os dados antes de enviar para base de dados
     * @param type $id
     * @param type $userName
     * @param type $password
     */
    public function setParam($id, $userName, $password) {
        $this->setId($id);
        $this->setUserName($userName);
        $this->setPassword($password);
    }

    public function load() {
        $resultSet = array();
        if (!empty($this->getId())) {
            $resultSet = AppDao::consultRegister("user", "id", $this->getId());
        }
        $this->setParam($resultSet["id"], $resultSet["user_name"], $resultSet["password"]);
    }

    public function validaInsert() {
        $msg = array();
        if (empty($this->getUserName())) {
            $msg[] = "O campo Usu�rio � obrigat�rio";
        }
        if (empty($this->getPassword())) {
            $msg[] = "O campo Senha � obrigat�rio";
        }
        return $msg;
    }

    public function insertData($values) {
        unset($values["submit"]);
        if (!empty($values)) {
            // Verifica se os campos necess�rios para o insert est�o aptos para inclus�o
            $msg = $this->validaInsert();
            if (empty($msg)) {
                // Gera o comando sql
                $sql = AppInstructionSql::insertSQL("user", $values);
                // Gera lista de atributos
                $attributos = AppInstructionSql::ajustarKeys($values);

                // Abre a conex�o com o banco de dados
                $db = new AppDb();
                $conn = $db->openConnect();
                try {
                    // Abre uma transa��o com o banco de dados
                    $db->beginTransaction();

                    // Prepara o comando sql
                    $smtp = $conn->prepare($sql);
                    $smtp->bindValue($attributos[0], $this->getUserName(), AppDao::returnParamPDO($this->getUserName()));
                    $smtp->bindValue($attributos[1], $this->getPassword(), AppDao::returnParamPDO($this->getPassword()));
                    // Executa o comando no banco
                    $smtp->execute();

                    // Retorna o id inserido no banco e seta no objeto
                    $this->setId($conn->lastInsertId());

                    // Comita a transa��o
                    $db->commit();
                    // Limpa os dados enviados via post
                    unset($values);
                    // Retorna o id inserido na opera��o
                    return $this->getId();
                } catch (\PDOException $exc) {
                    // Cancela e desfaz a transa��o com o banco caso ocorra erro
                    $db->rollback();
                    // Armazena informa��es do erro numa array
                    $msg[] = "<strong>C�digo:</strong> " . $exc->getCode();
                    $msg[] = "<strong>Arquivo:</strong> " . $exc->getFile();
                    $msg[] = "<strong>Linha:</strong> " . $exc->getLine();
                    $msg[] = "<strong>Mensagem:</strong> " . $exc->getMessage();
                    // Retorna Mensagem de erro
                    return $msg;
                }
            } else {
                // Retorna uma array contendo os erros que impediram o insert
                return $msg;
            }
        }
    }

    public function validaUpdate() {
        return $this->validaInsert();
    }

    public function updateData($values) {
        unset($values["submit"]);
        if (!empty($values)) {
            // Verifica se os campos necess�rios para o insert est�o aptos para inclus�o
            $msg = $this->validaUpdate();
            if (empty($msg)) {
                // Gera o comando sql
                $sql = AppInstructionSql::updatetSQL("user", $values);
                // Gera lista de atributos
                $attributos = AppInstructionSql::ajustarKeys($values);

                // Abre a conex�o com o banco de dados
                $db = new \application\lib\AppDb();
                $conn = $db->openConnect();
                try {
                    // Abre uma transa��o com o banco de dados
                    $db->beginTransaction();
                    // Prepara o comando sql
                    $smtp = $conn->prepare($sql);
                    $smtp->bindValue($attributos[0], $this->getUserName(), AppDao::returnParamPDO($this->getUserName()));
                    $smtp->bindValue($attributos[1], $this->getPassword(), AppDao::returnParamPDO($this->getPassword()));
                    $smtp->bindValue($attributos[2], $this->getId(), AppDao::returnParamPDO($this->getId()));
                    // Executa o comando no banco
                    $smtp->execute();
                    
                    // Comita a transa��o
                    $db->commit();
                    // Limpa os dados enviados via post
                    unset($values);
                    // Retorna o id inserido na opera��o
                    return $this->getId();
                } catch (\PDOException $exc) {
                    // Cancela e desfaz a transa��o com o banco caso ocorra erro
                    $db->rollback();
                    // Armazena informa��es do erro numa array
                    $msg[] = "<strong>C�digo:</strong> " . $exc->getCode();
                    $msg[] = "<strong>Arquivo:</strong> " . $exc->getFile();
                    $msg[] = "<strong>Linha:</strong> " . $exc->getLine();
                    $msg[] = "<strong>Mensagem:</strong> " . $exc->getMessage();
                    // Retorna Mensagem de erro
                    return $msg;
                }
            } else {
                return $msg;
            }
        }
    }

    public function validaUser() {
        if (!empty($this->getUserName()) && !empty($this->getPassword())) {
            $sql = "SELECT * FROM user WHERE user_name = '{$this->getUserName()}' AND password = '{$this->getPassword()}'";

            $validate = AppDao::consultRegisterBySql($sql);
            if (!empty($validate)) {
                AppSession::setValue("User", $validate["user_name"]);
                AppSession::setValue("User_Id", $validate["id"]);
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    /**
     * M�todo listBlog
     * Lista os registros
     * @return type array
     */
    public function listUser() {
        $sql = "SELECT * FROM user ORDER BY id ASC";
        $resultSet = AppDao::consultBySQL($sql);
        return $resultSet;
    }

    public function removeRegistro($table, $condictionAttribute, $condictionValue) {
        parent::removeRegistro($table, $condictionAttribute, $condictionValue);
    }

}
