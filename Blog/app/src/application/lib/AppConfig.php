<?php

/**
 * Description of Config
 * Classe respons�vel por fornecer os par�metros do sistema 
 * @author allan
 */

namespace application\lib;

class AppConfig {

    /**
     * M�todo deve ser configurado como true quando o servidor for linux
     * e false quando o servidor for windows
     * @return boolean
     */
    public static function typeServer() {
        return true;
        //return false;
    }

    /**
     * M�todo respons�vel pelo host do banco de dados
     * @return string - retorna uma string
     */
    public static function hostDb() {
        return "localhost";
    }

    /**
     * M�todo respons�vel pelo usuário do banco de dados
     * @return string - retorna uma string
     */
    public static function userDb() {
        return "root";
    }

    /**
     * M�todo respons�vel pela senha do banco de dados
     * @return string - retorna uma string
     */
    public static function pwdDb() {
        return "kerberos280104";
    }

    /**
     * M�todo respons�vel pelo nome do bando de dados
     * @return string - retorna uma string
     */
    public static function schema() {
        return "blog";
    }

    /**
     * M�todo respons�vel pelo PATH da aplica��o
     * @return string - retorna uma string
     */
    public static function UrlPath() {
        return "/";
    }

}

?>
