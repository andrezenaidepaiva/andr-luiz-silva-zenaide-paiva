<?php

/**
 * Description of InstructionSql
 * Classe respons�vel pela montagem de comando SQL
 * @author allan
 */

namespace application\lib;

class AppInstructionSql {

    /**
     * Retorna operador AND
     * @return string
     */
    public static function andOperation() {
        return " AND ";
    }

    /**
     * Retorna operador OR
     * @return string
     */
    public static function orOperation() {
        return " OR ";
    }

    /**
     * Retorna �rdem crescente
     * @return string
     */
    public static function crescente() {
        return " ASC ";
    }

    /**
     * Retorna �rdem decrescente
     * @return string
     */
    public static function decrescente() {
        return " DESC ";
    }

    /**
     * Retorna uma lista de orden��o
     * @return string
     */
    public static function listOrder() {
        $list = array();

        $list[0]["value"] = self::crescente();
        $list[0]["alias"] = "Crescente";
        $list[1]["value"] = self::decrescente();
        $list[1]["alias"] = "Decrescente";

        return $list;
    }

    /**
     * Retorna comparador igual
     * @return string
     */
    public static function igual() {
        return "Igual";
    }

    /**
     * Retorna comparador diferente
     * @return string
     */
    public static function diferente() {
        return "Diferente";
    }

    /**
     * Retorna consulta pelo termo inicial
     * @return string
     */
    public static function comecaCom() {
        return "Comeca Com";
    }

    /**
     * Retorna consulta pelo termo final
     * @return string
     */
    public static function terminaCom() {
        return "Termina Com";
    }

    /**
     * Retorna consulta pelo termo
     * @return string
     */
    public static function contem() {
        return "Contem";
    }

    /**
     * Retorna consulta que n�o cont�m o termo
     * @return string
     */
    public static function naoContem() {
        return "Nao Contem";
    }

    /**
     * Retorna comparador maior
     * @return string
     */
    public static function maior() {
        return "Maior";
    }

    /**
     * Retorna comparador menor
     * @return string
     */
    public static function menor() {
        return "Menor";
    }

    /**
     * Retorna comparador maior igual
     * @return string
     */
    public static function maiorIgual() {
        return "Maior Igual";
    }

    /**
     * Retorna comparador menor igal
     * @return string
     */
    public static function menorIgual() {
        return "Menor Igual";
    }

    /**
     * Retorna consulta entre termos
     * @return string
     */
    public static function entre() {
        return "Entre";
    }

    public static function listComparer() {
        $list = array();

        $list[0]["value"] = self::igual();
        $list[1]["value"] = self::diferente();
        $list[2]["value"] = self::maior();
        $list[3]["value"] = self::maiorIgual();
        $list[4]["value"] = self::menor();
        $list[5]["value"] = self::menorIgual();
        $list[6]["value"] = self::contem();
        $list[7]["value"] = self::naoContem();
        $list[8]["value"] = self::comecaCom();
        $list[9]["value"] = self::terminaCom();

        return $list;
    }

    /**
     * Retorna a compara��o para comando sql
     * @param type $attribute - atributo para compara��o
     * @param type $comparer - comparador da consulta
     * @param type $param - parametro do attribute
     * @return string - comando sql
     */
    public static function getComparer($attribute, $comparer, $param) {
        switch ($comparer) {
            case self::igual() :
                $sql = $attribute . " = '" . $param . "'";
                break;
            case self::diferente() :
                $sql = $attribute . " <> '" . $param . "'";
                break;
            case self::contem() :
                $sql = $attribute . " LIKE '%" . $param . "%'";
                break;
            case self::comecaCom() :
                $sql = $attribute . " LIKE '" . $param . "%'";
                break;
            case self::terminaCom() :
                $sql = $attribute . " LIKE '%" . $param . "'";
                break;
            case self::naoContem() :
                $sql = $attribute . " NOT LIKE '%" . $param . "%'";
                break;
            case self::maior() :
                $sql = $attribute . " > '" . $param . "'";
                break;
            case self::maiorIgual() :
                $sql = $attribute . " >= '" . $param . "'";
                break;
            case self::menor() :
                $sql = $attribute . " < '" . $param . "'";
                break;
            case self::menorIgual() :
                $sql = $attribute . " <= '" . $param . "'";
                break;
        }
        return $sql;
    }

    /**
     * Retorna comando sql para consulta
     * @param type $table - nome da tabela
     * @param type $attribute - atributo(s) consultados
     * @param type $comparer - tipo de comparador
     * @param type $param - parametro do attribute
     * @param type $operation - tipo de opera��o AND & OR
     * @param type $comparerBetween - consulta entre parametros inicial e final
     * @param type $attributeBetween - attributo para compara��o entre termos
     * @param type $paramFirst - parametro inicial
     * @param type $paramSecond - parametro final
     * @param type $attributeOrder - atributo para ordena��o
     * @param type $order - ordem crescente ou decrescente
     * @param type $limit - limita a quantidade de registros
     * @param type $offSet - indica onde come�a a listagem
     * @return string - comando sql
     */
    public static function montarSQL($table, $attribute, $comparer, $param, $operation, $operationBetween, $attributeBetween, $paramFirst, $paramSecond, $attributeOrder, $order, $limit, $offSet) {
        $sql = "SELECT " . $table . ".* FROM " . $table;
        if (is_array($attribute)) {
            $sql .= " WHERE ";
            for ($i = 0; $i < count($attribute); $i++) {
                if (AppSystem::_empty($attribute[$i])) {
                    $sql = "";
                    return $sql;
                } else {
                    if (AppSystem::_empty($comparer[$i])) {
                        $comparer[$i] = self::igual();
                    }
                    if ($i == 0 || $i < (count($attribute) - 1)) {
                        $sql .= self::getComparer($attribute[$i], $comparer[$i], $param[$i]);
                    } else {
                        if (is_array($operation)) {
                            $sql .= " " . $operation[$i] . " " . self::getComparer($attribute[$i], $comparer[$i], $param[$i]);
                        } else {
                            if (AppSystem::_empty($operation)) {
                                $operation = self::andOperation();
                            }
                            $sql .= " " . $operation . " " . self::getComparer($attribute[$i], $comparer[$i], $param[$i]);
                        }
                    }
                }
            }
        } else {
            if (AppSystem::_empty($attribute)) {
                $sql .= "";
            } else {
                $sql .= " WHERE ";
                $sql .= self::getComparer($attribute, $comparer, $param);
            }
        }

        if (!AppSystem::_empty($attributeOrder)) {
            if (AppSystem::_empty($order)) {
                $order = self::crescente();
            }
            $sql .= " " . self::getOrder($attributeOrder, $order);
        }

        //Caso as condi��es abaixo n�o sejam satisfeitas a condi��o between n�o ser� executada
        if (!AppSystem::_empty($attributeBetween) && AppSystem::_empty($paramSecond) && AppSystem::_empty($paramSecond)) {
            if (AppSystem::_empty($attribute)) {
                $sql .= " WHERE " . self::getBetween($attributeBetween, $paramFirst, $paramSecond);
            } else {
                if (AppSystem::_empty($operationBetween)) {
                    $operationBetween = self::andOperation();
                }
                $sql .= $operationBetween . self::getBetween($attributeBetween, $paramFirst, $paramSecond);
            }
        }
        //Caso haja limite
        if ($limit != "") {
            if (!AppSystem::_empty($offSet)) {
                $sql .= " " . self::getLimitOffSet($limit, $offSet);
            } else {
                $sql .= " " . self::getLimit($limit);
            }
        }

        return $sql;
    }

    /**
     * Retorna trecho de comando sql para consulta entre termos
     * @param type $attribute - atributo para compara��o
     * @param type $paramFirst - parametro inicial
     * @param type $paramSecond - parametro final
     * @return string - comando sql
     */
    public static function getBetween($attribute, $paramFirst, $paramSecond) {

        $sql = " BETWEEN " . $attribute . " ':" . $paramFirst . "' AND ':" . $paramSecond;

        return $sql;
    }

    /**
     * Retorna trecho de comando sql para limitar quantidade de registros por consulta
     * @param type $limit - valor para limitar a consulta
     * @return string - comando sql
     */
    public static function getLimit($limit) {

        $sql = " LIMIT " . $limit;

        return $sql;
    }

    /**
     * Retorna trecho de comando sql para limitar quantidade de registros por 
     * consulta a partir de um determinado registro
     * @param type $limit - limita a quantidade de registros
     * @param type $offSet - indica onde come�a a listagem
     * @return string - comando sql
     */
    public static function getLimitOffSet($limit, $offSet) {

        $sql = " LIMIT " . $limit . " OFFSET " . $offSet;

        return $sql;
    }

    /**
     * Retorna trecho do comando sql para ordenar a consulta
     * @param type $attribute - atributo para ordena��o
     * @param type $order - ordem crescente ou decrescente
     * @return string - comando sql
     */
    public static function getOrder($attribute, $order) {

        switch ($order) {
            case self::crescente() :
                $sql = " ORDER BY " . $attribute . " ASC";
                break;
            case self::decrescente() :
                $sql = " ORDER BY " . $attribute . " DESC";
                break;
        }
        return $sql;
    }

    public static function insertSQL($table, $values) {
        if (empty($table)) {
            $msg[] = "O nome da tabela deve ser definido";
        }
        if (empty($values)) {
            $msg[] = "A chave e os valores devem ser definidos";
        }
        if (empty($msg)) {
            $attributos = array_keys($values);
            $sql = "INSERT INTO " . $table . " (";
            $sql .= implode(", ", $attributos) . ") ";
            $sql .= "VALUES (:" . implode(", :", $attributos) . ")";
            return $sql;
        } else {
            return $msg;
        }
    }

    /**
     * M�todo newUpdatetSQL2
     * @param type $table - Nome da Tabela
     * @param type $values - Valores dos atributos
     * @return string - Retorna o comando SQL
     */
    public static function updatetSQL($table, $values) {
        if (empty($table)) {
            $msg[] = "O nome da tabela deve ser definido";
        }
        if (empty($values)) {
            $msg[] = "A chave e os valores devem ser definidos";
        }
        if (empty($msg)) {
            $attributos = array_keys($values);
            $sql = "UPDATE " . $table . " SET ";
            for ($i = 0; $i < count($attributos); $i++) {
                if ($i == 0) {
                    $sql .= $attributos[$i] . " = :" . $attributos[$i];
                } else {
                    $sql .= ", " . $attributos[$i] . " = :" . $attributos[$i];
                }
            }
            $sql .= " WHERE id = :id";
            return $sql;
        } else {
            return $msg;
        }
    }

    /**
     * Fun��o que retorna uma sql para consultar um registro para atualiza��o / visualiza��o
     * @param type $table - nome da tabela
     * @param type $conditionAttribute - atributo para consulta
     * @param type $conditionValue - parametro para consulta 
     * @return type - retorna uma lista de dados
     */
    public static function selectDataByUpdate($table, $condictionAttribute, $condictionValue) {

        $condictionValue = "'" . utf8_encode($condictionValue) . "'";
        $sql = "SELECT {$table}.* FROM {$table} WHERE {$condictionAttribute} = {$condictionValue}";

        return $sql;
    }

    /**
     * Retorna comando SQL para executar DELETE
     * @param type $table - Nome da Tabela
     * @param type $condictionAttribute - Nome do atributo condicional
     * @return string comando SQL comando SQL
     */
    public static function deleteSQL($table, $condictionAttribute, $condictionValue) {

        if (!is_integer($condictionValue)) {
            $condictionValue = "'" . $condictionValue . "'";
        }

        $sql = "DELETE FROM {$table} WHERE " . $condictionAttribute . " = " . $condictionValue;

        return $sql;
    }

    /**
     * Metodo ajustarKeys
     * Pega as chaves de um array
     * @param type $values
     * @return um array com as chaves do array especificado anteriormente
     */
    public static function ajustarKeys($values) {
        $attribs = array_keys($values);
        $i = 0;
        foreach ($attribs as $value) {
            $attribs[$i] = ":" . $value;
            $i++;
        }
        return $attribs;
    }

}

?>
