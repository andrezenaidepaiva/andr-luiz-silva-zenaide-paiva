<?php

/**
 * Description of System
 * Classe para tratar dados
 * @author allan
 */

namespace application\lib;

class AppSystem {

    public static function _empty($value) {
        return empty($value) && !is_numeric($value);
    }

    /**
     * Converte texto para tag html
     * @param type $value
     * @return type
     */
    public static function textByHtml($value) {
        $value = trim(html_entity_decode(stripslashes($value), ENT_QUOTES, 'ISO-8859-1'));
        return $value;
    }

    /**
     * Converte caracteres especiais para HTML
     * @param type $value
     * @return type
     */
    public static function htmlByText($value) {
        $value = trim(htmlspecialchars(stripslashes($value), ENT_QUOTES, 'ISO-8859-1'));
        return $value;
    }

    public static function _confirmRedirect($msg, $url) {

       
        $content .= "<div id ='Msg' class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>";
        $content .= "<div class='modal-dialog'>";
        $content .= "<div class='modal-content'>";
        $content .= "<div class='modal-header'>";
        $content .= "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
        $content .= "<h4 class='modal-title' id='myModalLabel'>Mensagem</h4>";
        $content .= "</div>";
        $content .= "<div class='modal-body'>";
        $content .= "<p><i class='icon-ok'></i> {$msg}</p>";
        $content .= "</div>";
        $content .= "<div class='modal-footer'>";
        $content .= "<a class='btn btn-primary' href='{$url}'>Ok</a>";
        $content .= "</div>";
        $content .= "</div>";
        $content .= "</div>";
        $content .= "</div>";

        print $content;
    }

    public static function _alertInformationRedirect($msg, $url) {

        $content .= "<div id ='Msg' class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>";
        $content .= "<div class='modal-dialog'>";
        $content .= "<div class='modal-content'>";
        $content .= "<div class='modal-header'>";
        $content .= "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
        $content .= "<h4 class='modal-title' id='myModalLabel'>Informa��o</h4>";
        $content .= "</div>";
        $content .= "<div class='modal-body'>";
        $content .= "<p><i class='icon-ok'></i> {$msg}</p>";
        $content .= "</div>";
        $content .= "<div class='modal-footer'>";
        $content .= "<a class='btn btn-primary' href='{$url}'>Ok</a>";
        //$content .= "<a class='btn' data-dismiss='modal' aria-hidden='true' href='{$url}'>Fechar</a>";
        $content .= "</div>";
        $content .= "</div>";
        $content .= "</div>";
        $content .= "</div>";

        print $content;
    }

    public static function _alertInformation($msg) {

        $content .= "<div id ='Msg' class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>";
        $content .= "<div class='modal-dialog'>";
        $content .= "<div class='modal-content'>";
        $content .= "<div class='modal-header'>";
        $content .= "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
        $content .= "<h4 class='modal-title' id='myModalLabel'>Informa��o</h4>";
        $content .= "</div>";
        $content .= "<div class='modal-body'>";
        $content .= "<p><i class='icon-ok'></i> {$msg}</p>";
        $content .= "</div>";
        $content .= "<div class='modal-footer'>";
        $content .= "<a class='btn' data-dismiss='modal' aria-hidden='true'>Fechar</a>";
        $content .= "</div>";
        $content .= "</div>";
        $content .= "</div>";
        $content .= "</div>";

        print $content;
    }

    public static function _alertErro($msg) {
       $content .= "<div id ='Msg' class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>";
        $content .= "<div class='modal-dialog'>";
        $content .= "<div class='modal-content'>";
        $content .= "<div class='modal-header'>";
        $content .= "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
        $content .= "<h4 class='modal-title' id='myModalLabel'>Aten��o</h4>";
        $content .= "</div>";
        $content .= "<div class='modal-body'>";
        $content .= "<p><i class='icon-exclamation'></i> {$msg}</p>";
        $content .= "</div>";
        $content .= "<div class='modal-footer'>";
        $content .= "<a class='btn' data-dismiss='modal' aria-hidden='true'>Fechar</a>";
        $content .= "</div>";
        $content .= "</div>";
        $content .= "</div>";
        $content .= "</div>";
        print $content;
    }

    public static function _redirect($url) {
        header("Location: " . $url);
        print '<html><head><meta http-equiv="refresh" content="0;URL=\'' . $url . '\'" /></head><body><a href="' . $url . '">Clique aqui para redirecionar...</a></body></html>';
        exit();
    }

    /**
     * Metodo _tratarMensagem()
     * @param type $mensagem mensagem em array ou em string
     * @return string mensagem tratada para box de alerta
     */
    public static function _tratarMensagem($mensagem) {
        if (is_array($mensagem)) {
            $msg .= "<ul>";
            foreach ($mensagem as $value) {
                $msg .= "<li>";
                $msg .= $value;
                $msg .= "</li>";
            }
            $msg .= "</ul>";
        } else {
            $msg .= "<ul>";
            $msg .= "<li>";
            $msg .= $mensagem;
            $msg .= "</li>";
            $msg .= "</ul>";
        }
        return $msg;
    }

    /**
     * Metodo _alertMensagem()
     * @param type $titulo - titulo da mensagem
     * @param type $mensagem - mensagem para exibicao
     */
    public static function _alertMensagem($titulo, $mensagem) {
        $msg = self::_tratarMensagem($mensagem);

        $content .= "<div id ='Msg' class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>";
        $content .= "<div class='modal-dialog'>";
        $content .= "<div class='modal-content'>";
        $content .= "<div class='modal-header'>";
        $content .= "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
        $content .= "<h4 class='modal-title' id='myModalLabel'>Informa��o</h4>";
        $content .= "</div>";
        $content .= "<div class='modal-body'>";
        $content .= "<p><strong><i class='icon-exclamation'></i> {$titulo}</strong></p>";
        $content .= "<p>{$msg}</p>";
        $content .= "</div>";
        $content .= "<div class='modal-footer'>";
        $content .= "<a class='btn' data-dismiss='modal' aria-hidden='true'>Fechar</a>";
        $content .= "</div>";
        $content .= "</div>";
        $content .= "</div>";
        $content .= "</div>";
        print $content;
    }

    /**
     * Metodo formataData()
     * Metodo para formatar data
     * @param type $data    - data a ser formatada
     * @param type $formato - formato para a data
     * @return a data formatada
     */
    public static function formatarData($data, $formato = "d/m/Y H:i:s") {

        //Valida: Y-m-d H:i:s
        $expdata = "/^(([0-9]{4})\-([0-9]{1,2})\-([0-9]{1,2})){1}[[:space:]](([0-9]{2}):([0-9]{2}):([0-9]{2})){1}$/";

        if (preg_match($expdata, $data)) {

            $array = explode(" ", $data);
            $adata = explode("-", $array[0]);
            $ahora = explode(":", $array[1]);

            $strdata = $formato;
            $strdata = preg_replace("{y}", substr($adata[0], 2, 2), $strdata);
            $strdata = preg_replace("{Y}", $adata[0], $strdata);
            $strdata = preg_replace("{m}", $adata[1], $strdata);
            $strdata = preg_replace("{d}", $adata[2], $strdata);
            $strdata = preg_replace("{H}", $ahora[0], $strdata);
            $strdata = preg_replace("{i}", $ahora[1], $strdata);
            $strdata = preg_replace("{s}", $ahora[2], $strdata);

            return $strdata;
        }

        //Valida: d/m/Y H:i:s
        $expdata = "/^(([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})){1}[[:space:]](([0-9]{2}):([0-9]{2}):([0-9]{2})){1}$/";

        if (preg_match($expdata, $data)) {

            $array = explode(" ", $data);
            $adata = explode("/", $array[0]);
            $ahora = explode(":", $array[1]);

            $strdata = $formato;
            $strdata = preg_replace("{y}", substr($adata[2], 2, 2), $strdata);
            $strdata = preg_replace("{Y}", $adata[2], $strdata);
            $strdata = preg_replace("{m}", $adata[1], $strdata);
            $strdata = preg_replace("{d}", $adata[0], $strdata);
            $strdata = preg_replace("{H}", $ahora[0], $strdata);
            $strdata = preg_replace("{i}", $ahora[1], $strdata);
            $strdata = preg_replace("{s}", $ahora[2], $strdata);

            return $strdata;
        }

        //Valida: Y-m-d
        $expdata = "/^(([0-9]{4})\-([0-9]{1,2})\-([0-9]{1,2})){1}$/";

        if (preg_match($expdata, $data)) {

            $adata = explode("-", $data);

            $strdata = $formato;
            $strdata = preg_replace("{y}", substr($adata[0], 2, 2), $strdata);
            $strdata = preg_replace("{Y}", $adata[0], $strdata);
            $strdata = preg_replace("{m}", $adata[1], $strdata);
            $strdata = preg_replace("{d}", $adata[2], $strdata);

            return $strdata;
        }

        //Valida: d/m/Y
        $expdata = "/^(([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})){1}$/";

        if (preg_match($expdata, $data)) {

            $adata = explode("/", $data);

            $strdata = $formato;
            $strdata = preg_replace("{y}", substr($adata[2], 2, 2), $strdata);
            $strdata = preg_replace("{Y}", $adata[2], $strdata);
            $strdata = preg_replace("{m}", $adata[1], $strdata);
            $strdata = preg_replace("{d}", $adata[0], $strdata);

            return $strdata;
        }

        //Valida: H:i:s
        $expdata = "/^(([0-9]{2}):([0-9]{2}):([0-9]{2})){1}$/";

        if (preg_match($expdata, $data)) {

            $ahora = explode(":", $data);

            $strdata = $formato;
            $strdata = preg_replace("{H}", $ahora[0], $strdata);
            $strdata = preg_replace("{i}", $ahora[1], $strdata);
            $strdata = preg_replace("{s}", $ahora[2], $strdata);

            return $strdata;
        }

        return null;
    }

    public static function slug($value) {
        $result = ereg_replace("[^a-zA-Z0-9_]", "", strtr($value, "�������������������������� ", "aaaaeeiooouucAAAAEEIOOOUUC_"));
        $result = strtr($result, "_", "-");
        $result = strtolower($result);

        return $result;   
    }

}

?>
