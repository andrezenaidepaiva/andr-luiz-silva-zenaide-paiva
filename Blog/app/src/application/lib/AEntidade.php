<?php

namespace application\lib;

/**
 * Description of AEntidade
 * Classe pai para todas as entidades que tratam operações no banco de dados
 * @author allan roberto
 */
abstract class AEntidade {

    private $id;

    /**
     * Metodo getId
     * Retorna o valor do id
     * @return type
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Metodo setId
     * Atribui valor ao $id
     * @param type $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * Metodo insertData
     * Metodo responsavel pela insercao na base de dados
     * @return type - codigo do id inserido na base de dados
     */
    abstract function insertData($values);

    /**
     * Metodo updateData
     * Metodo responsavel pela atualizacao de registros na base de dados
     * @return type - codigo do id atualizado na base de dados
     */
    abstract function updateData($values);

    /**
     * Metodo load
     * Responsavel por popular um objeto consultado pelo id
     * @return type - Retorna um objeto populado com dados
     */
    abstract function load();
    
    /**
     * Metodo responsavel pela validacao dos dados antes de efetuar a
     * insercao dos dados
     * @return type - Retorna um array contendo mensagem de erro (caso exista)
     */
    abstract function validaInsert();
    
    /**
     * Metodo responsavel pela validacao dos dados antes de efetuar a 
     * atualizacao dos dados
     * @return type - Retorna um array contendo mensagem de erro (caso exista)
     */
    abstract function validaUpdate();

    /**
     * Metodo executeOperation
     * @param type $values - Valores para inserir ou alterar na base de dados
     * @return type - Retorna o id do registro tratado
     */
    final function executeOperation($values) {
        if (!empty($this->getId())) {
            $result = $this->updateData($values);
        } else {
            $result = $this->insertData($values);
        }
        return $result;
    }
    /**
     * M�todo removeRegistro
     * @param type $table - nome da tabela
     * @param type $condictionAttribute - atritubo para excluir
     * @param type $condictionValue - valor para exclus�o
     * @return type quantidade de linhas afetadas
     */
    function removeRegistro($table, $condictionAttribute, $condictionValue) {
        return AppDao::deleteData($table, $condictionAttribute, $condictionValue);
    }

}
