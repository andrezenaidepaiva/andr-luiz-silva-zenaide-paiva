<?php

/**
 * Description of Db
 * Classe respons�vel pela conex�o com o banco de dados
 * @author allan
 */

namespace application\lib;

class AppDb {

    /**
     * Vari�veis necess�rias para efetuar a conex�o com o banco de dados
     * @var type 
     */
    private $host;
    private $db;
    private $strConn;
    private $user;
    private $pwd;
    private $connDB;

    public function __clone() {
        
    }

    public function __destruct() {
        
    }

    /**
     * M�todo para abrir a conex�o com o banco de dados
     */
    public function openConnect() {
        $this->host = AppConfig::hostDb();
        $this->db = AppConfig::schema();
        $this->strConn = 'mysql:host=' . $this->host . ';dbname=' . $this->db;
        $this->user = AppConfig::userDb();
        $this->pwd = AppConfig::pwdDb();

        try {
            $this->connDB = new \PDO($this->strConn, $this->user, $this->pwd);
            $this->connDB->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $exc) {
            die("Erro: <code>" . $exc->getMessage() . "</code>");
        }
        return $this->connDB;
    }

    /*
     * M�todo beginTransaction()
     * Abre uma transa��o
     */
    public function beginTransaction() {
        if ($this->connDB) {
            // Inicia a transa��o
            $this->connDB->beginTransaction();
        } 
    }
    
    /**
     * M�todo commit()
     * Conclui a transa��o no banco de dados
     */
    public function commit() {
        if ($this->connDB) {
            // Conclui a transa��o
            $this->connDB->commit();
            // Fecha a conex�o com o banco de dados
            $this->closeConnect();
        } 
    }
    
    /**
     * M�todo rollback()
     * Cancela a transa��o com o banco de dados
     */
    public function rollback() {
        if ($this->connDB) {
            // Cancela a transa��o com o banco dedados
            $this->connDB->rollBack();
            // Fecha a conex�o com o banco de dados
            $this->closeConnect();
        }
    }
    
    
    
    /**
     * M�todo para fechar a conex�o com o banco de dados
     */
    public function closeConnect() {
        $this->connDB = null;
    }

}

?>
