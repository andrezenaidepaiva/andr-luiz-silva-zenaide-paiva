<?php

namespace application\lib;

/**
 * Description of _Session
 * Classe respons�vel pelo gerenciamento de sess�es
 * @author dev
 */
class AppSession {

    /**
     * m�todo setValue()
     * @param type $var - nome da vari�vel
     * @param type $value - valor armazenado
     */
    public static function setValue($var, $value) {
        $_SESSION[$var] = $value;
    }

    /**
     * m�todo getValue()
     * @param type $var - nome da vari�vel
     * @return retorna o valor armazenado na se��o
     */
    public static function getValue($var) {
        if (isset($_SESSION[$var])) {
            return $_SESSION[$var];
        }
    }

    /**
     * m�todo freeSession()
     * destr�i a se��o
     */
    public static function freeSession() {
        $var = array_keys($_SESSION);
        for ($i = 0; $i < count($var); $i++) {
            unset($_SESSION[$var[$i]]);
        }
    }

}

?>
