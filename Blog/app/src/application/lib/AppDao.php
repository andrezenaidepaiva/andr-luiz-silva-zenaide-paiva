<?php

/**
 * Description of Model
 * Classe pai de todos os models contendo metodos 
 * para executar scripts no banco de dados
 * @author allan
 */

namespace application\lib;

class AppDao {

    /**
     * Metodo consult
     * Executa uma consulta
     * @param type $table - nome da tabela
     * @param type $attribute - atributo(s) consultados
     * @param type $comparer - tipo de comparador
     * @param type $param - parametro do attribute
     * @param type $operation - tipo de operacao AND & OR
     * @param type $comparerBetween - consulta entre parametros inicial e final
     * @param type $attributeBetween - attributo para comparacao entre termos
     * @param type $paramFirst - parametro inicial
     * @param type $paramSecond - parametro final
     * @param type $attributeOrder - atributo para ordenacao
     * @param type $order - ordem crescente ou decrescente
     * @param type $limit - limita a quantidade de registros
     * @param type $offSet - indica onde comeca a listagem
     * @return string - retorna uma lista de registros
     */
    public static function consult($table, $attribute, $comparer, $param, $operation, $operationBetween, $attributeBetween, $paramInicial, $paramFinal, $attributeOrder, $order, $limit, $offSet) {
        $sql = AppInstructionSql::montarSQL($table, $attribute, $comparer, $param, $operation, $operationBetween, $attributeBetween, $paramInicial, $paramFinal, $attributeOrder, $order, $limit, $offSet);

        if (empty($sql)) {
            $value = "";
        } else {
            try {
                $db = new AppDb();
                $conn = $db->openConnect();
                $smtp = $conn->query($sql);
                $value = $smtp->fetchAll();
            } catch (\PDOException $exc) {
                echo "Cod.:" . $exc->getCode() . " - " . $exc->getMessage();
            }
        }
        return $value;
    }

    /**
     * Metodo countConsult
     * Efetua contagem de registros em uma consulta
     * @param type $table - nome da tabela
     * @param type $attribute - atributo(s) consultados
     * @param type $comparer - tipo de comparador
     * @param type $param - parametro do attribute
     * @param type $operation - tipo de operacao AND & OR
     * @param type $comparerBetween - consulta entre parametros inicial e final
     * @param type $attributeBetween - attributo para comparacao entre termos
     * @param type $paramFirst - parametro inicial
     * @param type $paramSecond - parametro final
     * @param type $attributeOrder - atributo para ordenacao
     * @param type $order - ordem crescente ou decrescente
     * @param type $limit - limita a quantidade de registros
     * @param type $offSet - indica onde come?a a listagem
     * @return string - retorna quantidade de dados
     */
    public static function countConsult($table, $attribute, $comparer, $param, $operation, $operationBetween, $attributeBetween, $paramInicial, $paramFinal, $attributeOrder, $order, $limit, $offSet) {
        $sql = AppInstructionSql::montarSQL($table, $attribute, $comparer, $param, $operation, $operationBetween, $attributeBetween, $paramInicial, $paramFinal, $attributeOrder, $order, $limit, $offSet);

        if (empty($sql)) {
            $count = 0;
        } else {
            try {
                $db = new AppDb();
                $conn = $db->openConnect();
                $smtp = $conn->prepare($sql);
                $smtp->execute();
                $count = $smtp->rowCount();
            } catch (\PDOException $exc) {
                echo "Cod.:" . $exc->getCode() . " - " . $exc->getMessage();
            }
        }
        return $count;
    }

    /**
     * Executa uma consulta
     * @param type $table - sql
     * @return type - retorna uma lista de dados
     */
    public static function consultBySQL($sql) {
        try {
            $db = new AppDb();
            $conn = $db->openConnect();
            $smtp = $conn->query($sql);
            $value = $smtp->fetchAll();
        } catch (\PDOException $exc) {
            echo "Cod.:" . $exc->getCode() . " - " . $exc->getMessage();
        }
        return $value;
    }

    /**
     * Efetua contagem de registros em uma consulta 
     * @param type $sql - comando SQL
     * @return string - retorna quantidade de dados
     */
    public static function countConsultBySQL($sql) {
        if (empty($sql)) {
            $count = 0;
        } else {
            try {
                $db = new AppDb();
                $conn = $db->openConnect();
                $smtp = $conn->prepare($sql);
                $smtp->execute();
                $count = $smtp->rowCount();
            } catch (\PDOException $exc) {
                echo "Cod.:" . $exc->getCode() . " - " . $exc->getMessage();
            }
        }
        return $count;
    }

    /**
     * Metodo consultRegister
     * Consulta um unico registro
     * @param type $table - Tabela do banco de dados
     * @param type $condictionAttribute - Atributo para condicao
     * @param type $condictionValue - Valor para o atributo
     * @return type - retorna um registro
     */
    public static function consultRegister($table, $condictionAttribute, $condictionValue) {
        $sql = AppInstructionSql::selectDataByUpdate($table, $condictionAttribute, $condictionValue);
        if (empty($sql)) {
            $value = "";
        } else {
            try {
                $db = new AppDb();
                $conn = $db->openConnect();
                $smtp = $conn->query($sql);
                $value = $smtp->fetch(\PDO::FETCH_ASSOC);
            } catch (\PDOException $exc) {
                echo "Cod.:" . $exc->getCode() . " - " . $exc->getMessage();
            }
        }
        return $value;
    }

    /**
     * Metodo consultRegisterBySql
     * Consulta um unico registro
     * @param type $sql - sql para consultar registro
     * @return type - retorna um registro
     */
    public static function consultRegisterBySql($sql) {
        if (empty($sql)) {
            $value = "";
        } else {
            try {
                $db = new AppDb();
                $conn = $db->openConnect();
                $smtp = $conn->query($sql);
                $value = $smtp->fetch(\PDO::FETCH_ASSOC);
            } catch (\PDOException $exc) {
                echo "Cod.:" . $exc->getCode() . " - " . $exc->getMessage();
            }
        }
        return $value;
    }
    
    /**
     * Metodo deleteData
     * Funcao responsavel por apagar um registro da tabela 
     * @param type $table - nome da tabela
     * @param type $condictionAttribute - atributo condicional
     * @param type $condictionValue - valor condicional
     * @return type - retorna a quantidade de registros afetados
     */
    public static function deleteData($table, $condictionAttribute, $condictionValue) {
        $sql = AppInstructionSql::deleteSQL($table, $condictionAttribute, $condictionValue);
        $strConn = new AppDb();
        $conn = $strConn->openConnect();
        try {
            $smtp = $conn->Query($sql);
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

        return $smtp->rowCount();
    }

    /**
     * Metodo deleteDataBySQL()
     * Exclui registro via comando SQL
     * @param type $sql - comando sql para executar no banco de dados
     * @return integer - n?mero de linhas afetadas pelo comando 
     */
    public static function deleteDataBySQL($sql) {
        $strConn = new AppDb();
        $conn = $strConn->openConnect();
        try {
            $smtp = $conn->Query($sql);
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

        return $smtp->rowCount();
    }

    /**
     * Metodo returnParamPDO
     * Fun��o para retornar parametro correto para transacao
     * @param type $value para ser testado para retornar parametro correto PDO
     * @return type
     */
    public static function returnParamPDO($value) {
        if (is_integer($value)) {
            $param = \PDO::PARAM_INT;
        } else {
            $param = \PDO::PARAM_STR;
        }
        return $param;
    }
}

?>
