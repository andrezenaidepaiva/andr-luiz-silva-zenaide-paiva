<?php

namespace application\BlogCategoria;

use application\lib\AEntidade;
use application\lib\AppSystem;
use application\lib\AppDao;
use application\lib\AppDb;
use application\lib\AppInstructionSql;

/**
 * Description of BlogCategoria
 * Classe responsavel pela entidade da categoria blog 
 * @author allan roberto
 */
class BlogCategoria extends AEntidade {

    private $title;
    private $slug;

    /**
     * M�todo getTitle
     * @return type $title
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * M�todo getSlug
     * @return type $slug
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * M�todo setTitulo
     * Trata o campo t�tulo
     * @param type $title
     */
    public function setTitle($title) {
        $this->title = AppSystem::htmlByText($title);
    }

    /**
     * M�todo setSlug
     * Trata o slug a partir do t�tulo previamente setado
     */
    public function setSlug() {
        if (!empty($this->getTitle())) {
            $slug = AppSystem::slug($this->getTitle());
        }
        $this->slug = $slug;
    }

    /**
     * Metodo __toString
     * @return type attributo a ser retornado de forma padr�o pela classe
     */
    public function __toString() {
        return $this->getTitle();
    }

    /**
     * Metodo setParam
     * Responsavel por tratar todos os dados antes de enviar para base de dados
     * @param type $id
     * @param type $title
     * @param type $slug
     */
    public function setParam($id, $title) {
        $this->setId($id);
        $this->setTitle($title);
        $this->setSlug();
    }

    public function load() {
        $resultSet = array();
        if (!empty($this->getId())) {
            $resultSet = AppDao::consultRegister("blog_categoria", "id", $this->getId());
        }
        $this->setParam($resultSet["id"], $resultSet["title"]);
    }

    public function validaInsert() {
        $msg = array();
        if (empty($this->getTitle())) {
            $msg[] = "O campo Titulo � Obrigat�rio";
        }
        return $msg;
    }

    public function insertData($values) {
        unset($values["submit"]);
        if (!empty($values)) {
            // Verifica se os campos necess�rios para o insert est�o aptos para inclus�o
            $msg = $this->validaInsert();
            if (empty($msg)) {
                // Gera o comando sql
                $sql = AppInstructionSql::insertSQL("blog_categoria", $values);
                // Gera lista de atributos
                $attributos = AppInstructionSql::ajustarKeys($values);

                // Abre a conex�o com o banco de dados
                $db = new AppDb();
                $conn = $db->openConnect();
                try {
                    // Abre uma transa��o com o banco de dados
                    $db->beginTransaction();

                    // Prepara o comando sql
                    $smtp = $conn->prepare($sql);
                    $smtp->bindValue($attributos[0], $this->getTitle(), AppDao::returnParamPDO($this->getTitle()));
                    $smtp->bindValue($attributos[1], $this->getSlug(), AppDao::returnParamPDO($this->getSlug()));
                    // Executa o comando no banco
                    $smtp->execute();

                    // Retorna o id inserido no banco e seta no objeto
                    $this->setId($conn->lastInsertId());

                    // Comita a transa��o
                    $db->commit();
                    // Limpa os dados enviados via post
                    unset($values);
                    // Retorna o id inserido na opera��o
                    return $this->getId();
                } catch (\PDOException $exc) {
                    // Cancela e desfaz a transa��o com o banco caso ocorra erro
                    $db->rollback();
                    // Armazena informa��es do erro numa array
                    $msg[] = "<strong>C�digo:</strong> " . $exc->getCode();
                    $msg[] = "<strong>Arquivo:</strong> " . $exc->getFile();
                    $msg[] = "<strong>Linha:</strong> " . $exc->getLine();
                    $msg[] = "<strong>Mensagem:</strong> " . $exc->getMessage();
                    // Retorna Mensagem de erro
                    return $msg;
                }
            } else {
                // Retorna uma array contendo os erros que impediram o insert
                return $msg;
            }
        }
    }

    public function validaUpdate() {
        return $this->validaInsert();
    }

    public function updateData($values) {
        unset($values["submit"]);
        if (!empty($values)) {
            // Verifica se os campos necess�rios para o insert est�o aptos para inclus�o
            $msg = $this->validaUpdate();
            if (empty($msg)) {
                // Gera o comando sql
                $sql = AppInstructionSql::updatetSQL("blog_categoria", $values);
                // Gera lista de atributos
                $attributos = AppInstructionSql::ajustarKeys($values);

                // Abre a conex�o com o banco de dados
                $db = new \application\lib\AppDb();
                $conn = $db->openConnect();
                try {
                    // Abre uma transa��o com o banco de dados
                    $db->beginTransaction();
                    // Prepara o comando sql
                    $smtp = $conn->prepare($sql);
                    $smtp->bindValue($attributos[0], $this->getTitle(), AppDao::returnParamPDO($this->getTitle()));
                    $smtp->bindValue($attributos[1], $this->getSlug(), AppDao::returnParamPDO($this->getSlug()));
                    $smtp->bindValue($attributos[2], $this->getId(), AppDao::returnParamPDO($this->getId()));
                    // Executa o comando no banco
                    $smtp->execute();

                    // Comita a transa��o
                    $db->commit();
                    // Limpa os dados enviados via post
                    unset($values);
                    // Retorna o id inserido na opera��o
                    return $this->getId();
                } catch (\PDOException $exc) {
                    // Cancela e desfaz a transa��o com o banco caso ocorra erro
                    $db->rollback();
                    // Armazena informa��es do erro numa array
                    $msg[] = "<strong>C�digo:</strong> " . $exc->getCode();
                    $msg[] = "<strong>Arquivo:</strong> " . $exc->getFile();
                    $msg[] = "<strong>Linha:</strong> " . $exc->getLine();
                    $msg[] = "<strong>Mensagem:</strong> " . $exc->getMessage();
                    // Retorna Mensagem de erro
                    return $msg;
                }
            } else {
                return $msg;
            }
        }
    }

    /**
     * M�todo listBlogCategoria
     * Lista os registros
     * @return type array
     */
    public function listBlogCategoria() {
        $sql = "SELECT * FROM blog_categoria ORDER BY id ASC";
        $resultSet = AppDao::consultBySQL($sql);
        return $resultSet;
    }

    public function removeRegistro($table, $condictionAttribute, $condictionValue) {
        parent::removeRegistro($table, $condictionAttribute, $condictionValue);
    }

}
