<?php

namespace application\BlogComentario;

use application\lib\AEntidade;
use application\lib\AppSystem;
use application\lib\AppDao;
use application\lib\AppDb;
use application\lib\AppInstructionSql;
use application\Blog\Blog;

/**
 * Description of BlogComentario
 * Classe responsavel pela entidade da Comentario do blog
 * @author allan roberto
 */
class BlogComentario extends AEntidade {

    private $idBlog;
    private $author;
    private $comment;
    private $insertDate;
    private $status;

    /**
     * M�todo getIdBlog
     * @return type $idBlog
     */
    public function getIdBlog() {
        return $this->idBlog;
    }

    /**
     * M�todo getAuthor
     * @return type $author
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * M�todo getComment
     * @return type $coment
     */
    public function getComment() {
        return $this->comment;
    }

    /**
     * M�todo getInsertDate
     * @return type $insertDate
     */
    public function getInsertDate() {
        return $this->insertDate;
    }

    /**
     * M�todo getStatus
     * @return type $status
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * M�todo setIdBlog
     * Trata o campo id do post
     * @param type $idBlog
     */
    public function setIdBlog($idBlog) {
        $this->idBlog = AppSystem::htmlByText($idBlog);
    }

    /**
     * M�todo setAuthor
     * Trata o campo autor
     * @param type $author
     */
    public function setAuthor($author) {
        $this->author = AppSystem::htmlByText($author);
    }

    /**
     * M�todo setComment
     * Trata o campo Coment�rio
     * @param type $comment
     */
    public function setComment($comment) {
        $this->comment = AppSystem::htmlByText($comment);
    }

    /**
     * M�todo setInsertDate
     * Trata o campo insertDate
     * @param type $insertDate
     */
    public function setInsertDate($insertDate) {
        if (empty($insertDate)) {
            $insertDate = date("Y-m-d H:i:s");
        } else {
            $insertDate = AppSystem::formatarData($insertDate, "Y-m-d H:i:s");
        }
        $this->insertDate = $insertDate;
    }

    /**
     * M�todo setStatus
     * Trata o campo insertDate
     * @param type $status
     */
    public function setStatus($status) {
        if (empty($status)) {
            $status = 2;
        }
        $this->status = $status;
    }

    /**
     * Metodo __toString
     * @return type attributo a ser retornado de forma padr�o pela classe
     */
    public function __toString() {
        return $this->getComment();
    }

    /**
     * Metodo __get
     * @param type $attrib valor para ser interceptado quando invocado pela classe
     * @return um objeto 
     */
    public function __get($attrib) {
        if (!empty($attrib)) {
            switch ($attrib) {
                case "Blog":
                    if (!empty($this->getIdBlog())) {
                        $objBlog = new Blog();
                        $objBlog->setId($this->getIdBlog());
                        $objBlog->load();
                    } else {
                        $objBlog = "O c�digo da postagem � obrigat�rio";
                    }
                    return $objBlog;
                    break;
            }
        }
    }

    /**
     * Metodo setParam
     * Responsavel por tratar todos os dados antes de enviar para base de dados
     * @param type $id
     * @param type $idBlog
     * @param type $author
     * @param type $comment
     * @param type $insertDate
     * @param type $status
     */
    public function setParam($id, $idBlog, $author, $comment, $insertDate, $status) {
        $this->setId($id);
        $this->setIdBlog($idBlog);
        $this->setAuthor($author);
        $this->setComment($comment);
        $this->setInsertDate($insertDate);
        $this->setStatus($status);
    }

    public function load() {
        $resultSet = array();
        if (!empty($this->getId())) {
            $resultSet = AppDao::consultRegister("blog_comentario", "id", $this->getId());
        }
        $this->setParam($resultSet["id"], $resultSet["id_blog"], $resultSet["author"], $resultSet["comment"], $resultSet["insert_date"], $resultSet["status"]);
    }

    public function validaInsert() {
        $msg = array();
        if (empty($this->getIdBlog())) {
            $msg[] = "O campo Postagem deve ser selecionado";
        }
        if (empty($this->getComment())) {
            $msg[] = "O campo Coment�rio � obrigat�rio";
        }
        return $msg;
    }

    public function insertData($values) {
        unset($values["submit"]);
        if (!empty($values)) {
            // Verifica se os campos necess�rios para o insert est�o aptos para inclus�o
            $msg = $this->validaInsert();
            if (empty($msg)) {
                // Gera o comando sql
                $sql = AppInstructionSql::insertSQL("blog_comentario", $values);
                // Gera lista de atributos
                $attributos = AppInstructionSql::ajustarKeys($values);

                // Abre a conex�o com o banco de dados
                $db = new AppDb();
                $conn = $db->openConnect();
                try {
                    // Abre uma transa��o com o banco de dados
                    $db->beginTransaction();

                    // Prepara o comando sql
                    $smtp = $conn->prepare($sql);
                    $smtp->bindValue($attributos[0], $this->getIdBlog(), AppDao::returnParamPDO($this->getIdBlog()));
                    $smtp->bindValue($attributos[1], $this->getAuthor(), AppDao::returnParamPDO($this->getAuthor()));
                    $smtp->bindValue($attributos[2], $this->getComment(), AppDao::returnParamPDO($this->getComment()));
                    $smtp->bindValue($attributos[3], $this->getInsertDate(), AppDao::returnParamPDO($this->getInsertDate()));
                    $smtp->bindValue($attributos[4], $this->getStatus(), AppDao::returnParamPDO($this->getStatus()));
                    // Executa o comando no banco
                    $smtp->execute();

                    // Retorna o id inserido no banco e seta no objeto
                    $this->setId($conn->lastInsertId());

                    // Comita a transa��o
                    $db->commit();
                    // Limpa os dados enviados via post
                    unset($values);
                    // Retorna o id inserido na opera��o
                    return $this->getId();
                } catch (\PDOException $exc) {
                    // Cancela e desfaz a transa��o com o banco caso ocorra erro
                    $db->rollback();
                    // Armazena informa��es do erro numa array
                    $msg[] = "<strong>C�digo:</strong> " . $exc->getCode();
                    $msg[] = "<strong>Arquivo:</strong> " . $exc->getFile();
                    $msg[] = "<strong>Linha:</strong> " . $exc->getLine();
                    $msg[] = "<strong>Mensagem:</strong> " . $exc->getMessage();
                    // Retorna Mensagem de erro
                    return $msg;
                }
            } else {
                // Retorna uma array contendo os erros que impediram o insert
                return $msg;
            }
        }
    }

    public function validaUpdate() {
        return $this->validaInsert();
    }

    public function updateData($values) {
        unset($values["submit"]);
        if (!empty($values)) {
            // Verifica se os campos necess�rios para o insert est�o aptos para inclus�o
            $msg = $this->validaUpdate();
            if (empty($msg)) {
                // Gera o comando sql
                $sql = AppInstructionSql::updatetSQL("blog_comentario", $values);
                // Gera lista de atributos
                $attributos = AppInstructionSql::ajustarKeys($values);

                // Abre a conex�o com o banco de dados
                $db = new \application\lib\AppDb();
                $conn = $db->openConnect();
                try {
                    // Abre uma transa��o com o banco de dados
                    $db->beginTransaction();
                    // Prepara o comando sql
                    $smtp = $conn->prepare($sql);
                    $smtp->bindValue($attributos[0], $this->getIdBlog(), AppDao::returnParamPDO($this->getIdBlog()));
                    $smtp->bindValue($attributos[1], $this->getAuthor(), AppDao::returnParamPDO($this->getAuthor()));
                    $smtp->bindValue($attributos[2], $this->getComment(), AppDao::returnParamPDO($this->getComment()));
                    $smtp->bindValue($attributos[3], $this->getInsertDate(), AppDao::returnParamPDO($this->getInsertDate()));
                    $smtp->bindValue($attributos[4], $this->getStatus(), AppDao::returnParamPDO($this->getStatus()));
                    $smtp->bindValue($attributos[5], $this->getId(), AppDao::returnParamPDO($this->getIdBlog()));
                    // Executa o comando no banco
                    $smtp->execute();
                    
                    // Comita a transa��o
                    $db->commit();
                    // Limpa os dados enviados via post
                    unset($values);
                    // Retorna o id inserido na opera��o
                    return $this->getId();
                } catch (\PDOException $exc) {
                    // Cancela e desfaz a transa��o com o banco caso ocorra erro
                    $db->rollback();
                    // Armazena informa��es do erro numa array
                    $msg[] = "<strong>C�digo:</strong> " . $exc->getCode();
                    $msg[] = "<strong>Arquivo:</strong> " . $exc->getFile();
                    $msg[] = "<strong>Linha:</strong> " . $exc->getLine();
                    $msg[] = "<strong>Mensagem:</strong> " . $exc->getMessage();
                    // Retorna Mensagem de erro
                    return $msg;
                }
            } else {
                return $msg;
            }
        }
    }

    /**
     * M�todo listBlogComentario
     * Lista os registros
     * @return type array
     */
    public function listBlogComentario() {
        $sql = "SELECT * FROM blog_comentario ORDER BY id ASC";
        $resultSet = AppDao::consultBySQL($sql);
        return $resultSet;
    }

    public function removeRegistro($table, $condictionAttribute, $condictionValue) {
        parent::removeRegistro($table, $condictionAttribute, $condictionValue);
    }

}
