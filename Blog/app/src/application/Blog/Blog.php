<?php

namespace application\Blog;

/**
 * Description of Blog
 * Classe responsavel pela entidade do blog 
 * @author allan roberto
 */
use application\BlogCategoria\BlogCategoria;
use application\lib\AEntidade;
use application\lib\AppSystem;
use application\lib\AppDao;
use application\lib\AppDb;
use application\lib\AppInstructionSql;

class Blog extends AEntidade {

    private $idCategoria;
    private $title;
    private $slug;
    private $description;
    private $body;
    private $author;
    private $insertDate;
    private $updateDate;

    /**
     * Metodo __get
     * @param type $attrib valor para ser interceptado quando invocado pela classe
     * @return um objeto 
     */
    public function __get($attrib) {
        if (!empty($attrib)) {
            switch ($attrib) {
                case "Categoria":
                    if (!empty($this->getIdCategoria())) {
                        $objCategoria = new BlogCategoria();
                        $objCategoria->setId($this->getIdCategoria());
                        $objCategoria->load();
                    } else {
                        $objCategoria = "O c�digo da categoria deve ser selecionado";
                    }
                    return $objCategoria;
                    break;
            }
        }
    }

    /**
     * Metodo __toString
     * @return type attributo a ser retornado de forma padr�o pela classe
     */
    public function __toString() {
        return $this->getTitle();
    }

    /**
     * M�todo getIdCategoria
     * @return type $idCategoria
     */
    public function getIdCategoria() {
        return $this->idCategoria;
    }

    /**
     * M�todo getTitle
     * @return type $title
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * M�todo getSlug
     * @return type $slug
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * M�todo getDescription
     * @return type $description
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * M�todo getBody
     * @return type $body
     */
    public function getBody() {
        return $this->body;
    }

    /**
     * M�todo getAuthor
     * @return type $author
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * M�todo getInsertDate
     * @return type $insertDate
     */
    public function getInsertDate() {
        return $this->insertDate;
    }

    /**
     * M�todo getUpdateDate
     * @return type $updateDate
     */
    public function getUpdateDate() {
        return $this->updateDate;
    }

    /**
     * M�todo setIdCategoria
     * Trata o campo categoria do blog
     * @param type $idCategoria
     */
    public function setIdCategoria($idCategoria) {
        $this->idCategoria = AppSystem::htmlByText($idCategoria);
    }

    /**
     * M�todo setTitulo
     * Trata o campo t�tulo
     * @param type $title
     */
    public function setTitle($title) {
        $this->title = AppSystem::htmlByText($title);
    }

    /**
     * M�todo setSlug
     * Trata o slug a partir do t�tulo previamente setado
     */
    public function setSlug() {
        if (!empty($this->getTitle())) {
            $slug = AppSystem::slug($this->getTitle());
        }
        $this->slug = $slug;
    }

    /**
     * M�todo setDescription
     * Trata o campo descri��o
     * @param type $description
     */
    public function setDescription($description) {
        $this->description = AppSystem::htmlByText($description);
    }

    /**
     * M�todo setBody
     * Trata o campo texto
     * @param type $body
     */
    public function setBody($body) {
        $this->body = AppSystem::htmlByText($body);
    }

    /**
     * M�todo setAuthor
     * Trata o campo autor
     * @param type $author
     */
    public function setAuthor($author) {
        $this->author = AppSystem::htmlByText($author);
    }

    /**
     * M�todo setInsertDate
     * Trata o campo insertDate
     * @param type $insertDate
     */
    public function setInsertDate($insertDate) {
        if (empty($insertDate)) {
            $insertDate = date("Y-m-d H:i:s");
        } else {
            $insertDate = AppSystem::formatarData($insertDate, "Y-m-d H:i:s");
        }
        $this->insertDate = $insertDate;
    }

    /**
     * M�todo setUpdateDate
     * Trata o campo updateDate
     * @param type $updateDate
     */
    public function setUpdateDate($updateDate) {
        if (empty($updateDate)) {
            $updateDate = date("Y-m-d H:i:s");
        } else {
            $updateDate = AppSystem::formatarData($updateDate, "Y-m-d H:i:s");
        }
        $this->updateDate = $updateDate;
    }

    /**
     * Metodo setParam
     * Responsavel por tratar todos os dados antes de enviar para base de dados
     * @param type $id
     * @param type $idCategoria
     * @param type $title
     * @param type $slug
     * @param type $description
     * @param type $body
     * @param type $author
     * @param type $insertDate
     * @param type $updateDate
     */
    function setParam($id, $idCategoria, $title, $slug, $description, $body, $author, $insertDate, $updateDate) {
        $this->setId($id);
        $this->setIdCategoria($idCategoria);
        $this->setTitle($title);
        $this->setSlug();
        $this->setDescription($description);
        $this->setBody($body);
        $this->setAuthor($author);
        $this->setInsertDate($insertDate);
        $this->setUpdateDate($updateDate);
    }

    public function load() {
        $resultSet = array();
        if (!empty($this->getId())) {
            $resultSet = AppDao::consultRegister("blog", "id", $this->getId());
        }
        $this->setParam($resultSet["id"], $resultSet["id_categoria"], $resultSet["title"], $resultSet["slug"], $resultSet["description"], $resultSet["body"], $resultSet["author"], $resultSet["insert_date"], $resultSet["update_date"]);
    }

    public function validaInsert() {
        $msg = array();
        if (empty($this->getIdCategoria())) {
            $msg[] = "O campo Categoria � Obrigat�rio";
        }
        if (empty($this->getTitle())) {
            $msg[] = "O campo T�tulo � Obrigat�rio";
        }
        if (empty($this->getBody())) {
            $msg[] = "O campo Texto � Obrigat�rio";
        }
        if (empty($this->getAuthor())) {
            $msg[] = "O campo Autor � Obrigat�rio";
        }
        return $msg;
    }

    public function insertData($values) {
        unset($values["submit"]);
        if (!empty($values)) {
            // Verifica se os campos necess�rios para o insert est�o aptos para inclus�o
            $msg = $this->validaInsert();
            if (empty($msg)) {
                // Gera o comando sql
                $sql = AppInstructionSql::insertSQL("blog", $values);
                // Gera lista de atributos
                $attributos = AppInstructionSql::ajustarKeys($values);

                // Abre a conex�o com o banco de dados
                $db = new AppDb();
                $conn = $db->openConnect();
                try {
                    // Abre uma transa��o com o banco de dados
                    $db->beginTransaction();

                    // Prepara o comando sql
                    $smtp = $conn->prepare($sql);
                    $smtp->bindValue($attributos[0], $this->getIdCategoria(), AppDao::returnParamPDO($this->getIdCategoria()));
                    $smtp->bindValue($attributos[1], $this->getTitle(), AppDao::returnParamPDO($this->getTitle()));
                    $smtp->bindValue($attributos[2], $this->getSlug(), AppDao::returnParamPDO($this->getSlug()));
                    $smtp->bindValue($attributos[3], $this->getDescription(), AppDao::returnParamPDO($this->getDescription()));
                    $smtp->bindValue($attributos[4], $this->getBody(), AppDao::returnParamPDO($this->getBody()));
                    $smtp->bindValue($attributos[5], $this->getAuthor(), AppDao::returnParamPDO($this->getAuthor()));
                    $smtp->bindValue($attributos[6], $this->getInsertDate(), AppDao::returnParamPDO($this->getInsertDate()));
                    // Executa o comando no banco
                    $smtp->execute();

                    // Retorna o id inserido no banco e seta no objeto
                    $this->setId($conn->lastInsertId());

                    // Comita a transa��o
                    $db->commit();
                    // Limpa os dados enviados via post
                    unset($values);
                    // Retorna o id inserido na opera��o
                    return $this->getId();
                } catch (\PDOException $exc) {
                    // Cancela e desfaz a transa��o com o banco caso ocorra erro
                    $db->rollback();
                    // Armazena informa��es do erro numa array
                    $msg[] = "<strong>C�digo:</strong> " . $exc->getCode();
                    $msg[] = "<strong>Arquivo:</strong> " . $exc->getFile();
                    $msg[] = "<strong>Linha:</strong> " . $exc->getLine();
                    $msg[] = "<strong>Mensagem:</strong> " . $exc->getMessage();
                    // Retorna Mensagem de erro
                    return $msg;
                }
            } else {
                // Retorna uma array contendo os erros que impediram o insert
                return $msg;
            }
        }
    }

    public function validaUpdate() {
        return $this->validaInsert();
    }

    public function updateData($values) {
        unset($values["submit"]);
        if (!empty($values)) {
            // Verifica se os campos necess�rios para o insert est�o aptos para inclus�o
            $msg = $this->validaUpdate();
            if (empty($msg)) {
                // Gera o comando sql
                $sql = AppInstructionSql::updatetSQL("blog", $values);
                // Gera lista de atributos
                $attributos = AppInstructionSql::ajustarKeys($values);

                // Abre a conex�o com o banco de dados
                $db = new \application\lib\AppDb();
                $conn = $db->openConnect();
                try {
                    // Abre uma transa��o com o banco de dados
                    $db->beginTransaction();
                    // Prepara o comando sql
                    $smtp = $conn->prepare($sql);
                    $smtp->bindValue($attributos[0], $this->getIdCategoria(), AppDao::returnParamPDO($this->getIdCategoria()));
                    $smtp->bindValue($attributos[1], $this->getTitle(), AppDao::returnParamPDO($this->getTitle()));
                    $smtp->bindValue($attributos[2], $this->getSlug(), AppDao::returnParamPDO($this->getSlug()));
                    $smtp->bindValue($attributos[3], $this->getDescription(), AppDao::returnParamPDO($this->getDescription()));
                    $smtp->bindValue($attributos[4], $this->getBody(), AppDao::returnParamPDO($this->getBody()));
                    $smtp->bindValue($attributos[5], $this->getAuthor(), AppDao::returnParamPDO($this->getAuthor()));
                    $smtp->bindValue($attributos[6], $this->getUpdateDate(), AppDao::returnParamPDO($this->getUpdateDate()));
                    $smtp->bindValue($attributos[7], $this->getId(), AppDao::returnParamPDO($this->getId()));
                    // Executa o comando no banco
                    $smtp->execute();
                    
                    // Comita a transa��o
                    $db->commit();
                    // Limpa os dados enviados via post
                    unset($values);
                    // Retorna o id inserido na opera��o
                    return $this->getId();
                } catch (\PDOException $exc) {
                    // Cancela e desfaz a transa��o com o banco caso ocorra erro
                    $db->rollback();
                    // Armazena informa��es do erro numa array
                    $msg[] = "<strong>C�digo:</strong> " . $exc->getCode();
                    $msg[] = "<strong>Arquivo:</strong> " . $exc->getFile();
                    $msg[] = "<strong>Linha:</strong> " . $exc->getLine();
                    $msg[] = "<strong>Mensagem:</strong> " . $exc->getMessage();
                    // Retorna Mensagem de erro
                    return $msg;
                }
            } else {
                return $msg;
            }
        }
    }

    /**
     * M�todo listBlog
     * Lista os registros
     * @return type array
     */
    public function listBlog() {
        $sql = "SELECT * FROM blog ORDER BY id ASC";
        $resultSet = AppDao::consultBySQL($sql);
        return $resultSet;
    }

    public function removeRegistro($table, $condictionAttribute, $condictionValue) {
        parent::removeRegistro($table, $condictionAttribute, $condictionValue);
    }

}
