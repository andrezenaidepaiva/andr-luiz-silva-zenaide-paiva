<?php
require_once './config.php';

use application\lib\AppSystem;
use application\User\User;

// verifica se o formul�rio enviou dados via POST
if (!empty($_POST)) {
     // Setar os par�metros do objeto com os dados do formul�rio para que possam ser tratados simultaneamente
    $objUser = new User();
    $objUser->setParam("", $_POST["user_name"], $_POST["password"]);
     // Executar a valida��o dos dados
    if ($objUser->validaUser()) {
        AppSystem::_redirect("/blog.php");
    } else {
        $msg = "Usu�rio ou senha incorreta";
    }
} 
if (!empty($_SESSION["User"])) {
    AppSystem::_redirect("/blog.php");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="iso-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Start Bootstrap - SB Admin Version 2.0 Demo</title>

        <!-- Core CSS - Include with every page -->
        <link href="scripts/sb-admin-v2/css/bootstrap.min.css" rel="stylesheet">
        <link href="scripts/sb-admin-v2/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- SB Admin CSS - Include with every page -->
        <link href="scripts/sb-admin-v2/css/sb-admin.css" rel="stylesheet">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Autenticar Usu�rio</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" name="login" method="post">
                                <fieldset>
                                    <?php if (!empty($msg)) { ?>
                                        <div class="form-group">
                                            <div class="alert alert-danger">
                                                <?php echo $msg; ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Inserir Usu�rio" name="user_name" type="user_name" autofocus />
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Inserir Senha" name="password" type="password" value="" />
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit" class="btn btn-lg btn-success btn-block" value="Login" />
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Core Scripts - Include with every page -->
        <script src="scripts/sb-admin-v2/js/jquery-1.10.2.js"></script>
        <script src="scripts/sb-admin-v2/js/bootstrap.min.js"></script>
        <script src="scripts/sb-admin-v2/js/plugins/metisMenu/jquery.metisMenu.js"></script>

        <!-- SB Admin Scripts - Include with every page -->
        <script src="scripts/sb-admin-v2/js/sb-admin.js"></script>

    </body>

</html>




