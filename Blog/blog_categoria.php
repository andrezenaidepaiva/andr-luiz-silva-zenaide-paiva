<?php
require_once './config.php';

use application\BlogCategoria\BlogCategoria;

$objBlogCategoria = new BlogCategoria();
// Listando dados
$resultSet = $objBlogCategoria->listBlogCategoria();
?>
<?php include_once './header.php'; ?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Categorias</h1>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Listagem de Categorias <a href="/blog_categoria_operation.php" class="btn btn-success"><i class="fa fa-plus"> Novo</i></a>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>C�digo</th>
                                    <th>T�tulo</th>
                                    <th>Op��es</th>
                                </tr>
                            </thead>
                            <tbody>
                            <!-- Percorrendo registros da listagem caso o array n�o esteja vazio-->
                            <?php if (!empty($resultSet)) { ?>
                                <?php for ($i = 0; $i < count($resultSet); $i++) { ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $resultSet[$i]["id"]; ?></td>
                                        <td><?php echo $resultSet[$i]["title"]; ?></td>
                                        <td>
                                            <a href="/blog_categoria_operation.php?id=<?php echo $resultSet[$i]["id"]; ?>" class="btn btn-default btn-circle"><i class="fa fa-edit"></i></a>
                                            <a href="/blog_categoria_view.php?id=<?php echo $resultSet[$i]["id"]; ?>" class="btn btn-default btn-circle"><i class="fa fa-search"></i></a>
                                            <a href="/blog_categoria_delete.php?id=<?php echo $resultSet[$i]["id"]; ?>" class="btn btn-danger btn-circle"><i class="fa fa-eraser"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <tr class="odd gradeX">
                                    <td ></td>
                                    <td >N�o possui registro</td>
                                    <td ></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#wrapper -->
<?php include_once './footer.php'; ?>
<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>
<!-- Page-Level Plugin Scripts - Tables -->
<script src="scripts/sb-admin-v2/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="scripts/sb-admin-v2/js/plugins/dataTables/dataTables.bootstrap.js"></script> 
