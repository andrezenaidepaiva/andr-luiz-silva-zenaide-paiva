<?php
require_once './config.php';

use application\BlogComentario\BlogComentario;
use application\lib\AppSystem;

// Requisitando id para excluir registro
$id = $_REQUEST["id"];

$objBlogComentario = new BlogComentario();
// Removendo Registro
$objBlogComentario->removeRegistro("blog_comentario", "id", $id);
// Redirecionando para Listagem
AppSystem::_redirect("/blog_comentario.php");

?>
