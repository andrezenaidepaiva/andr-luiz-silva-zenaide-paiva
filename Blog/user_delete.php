<?php
require_once './config.php';

use application\User\User;
use application\lib\AppSystem;

// Requisitando id para excluir registro
$id = $_REQUEST["id"];

$objUser = new User();
// Removendo Registro
$objUser->removeRegistro("user", "id", $id);
// Redirecionando para Listagem
AppSystem::_redirect("/user.php");

?>
