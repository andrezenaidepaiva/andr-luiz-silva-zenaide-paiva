<?php
require_once './config.php';

use application\BlogComentario\BlogComentario;
use application\lib\AppSystem;

// Requisitando id para excluir registro
$id = $_REQUEST["id"];

$objBlogComentario = new BlogComentario();
// Setando vari�vel id no objeto
$objBlogComentario->setId($id);
// Consultando Registro no objeto
$objBlogComentario->load();
?>
<?php include_once './header.php'; ?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Visualizar Comentario</h1>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Visualizar Registro
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" name="form" method="post">
                                <div class="form-group">
                                    <label>C�digo: <?php echo $objBlogComentario->getId(); ?></label>
                                </div>
                                <div class="form-group">
                                    <label>Autor do Coment�rio: <?php echo $objBlogComentario->getAuthor(); ?></label>
                                </div>
                                <div class="form-group">
                                    <label>Postagem: <?php echo $objBlogComentario->Blog; ?></label>
                                </div>
                                <div class="form-group">
                                    <label>Coment�rio: <br />
                                        <?php echo AppSystem::textByHtml($objBlogComentario->getComment()); ?>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>Data do Coment�rio: <?php echo AppSystem::formatarData($objBlogComentario->getInsertDate(), "d/m/Y H:i:s"); ?></label>
                                </div>
                                <div class="form-group">
                                    <label>Status: <?php if ($objBlogComentario->getStatus() == 1) {
                                            echo "Liberado";
                                        } else {
                                            echo "Pendente";
                                        } ?></label>
                                </div>
                                <a href="/blog_comentario.php" class="btn btn-default">Retornar</a>
                            </form>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /#wrapper -->
<?php include_once './footer.php'; ?>