<?php
require_once './config.php';

use application\BlogComentario\BlogComentario;
use application\Blog\Blog;
use application\lib\AppSystem;

// Pegando id via 
$id = $_REQUEST["id"];
// Caso o id exista, preencher o objeto para popular os campos do formul�rio
$objBlogComentario = new BlogComentario();
if (!empty($id)) {
    $objBlogComentario->setId($id);
    $objBlogComentario->load();
}

// Listando registros do blog para carregar no select
$objBlog = new Blog();
$list = $objBlog->listBlog();

// verifica se o formul�rio enviou dados via POST
if (!empty($_POST)) {
    // Setar os par�metros do objeto com os dados do formul�rio para que possam ser tratados simultaneamente
    $objBlogComentario->setParam($_POST["id"], $_POST["id_blog"], $_POST["author"], $_POST["comment"], $_POST["insert_date"], $_POST["status"]);
    // Executa uma opera��o no banco de dados INSERT/UPDATE
    $result = $objBlogComentario->executeOperation($_POST);
    // Caso o retorno seja um array retornar uma mensagem de erro na tela
    // Do contr�rio concluir a opera��o e redirecionar para a p�gina principal do m�dulo
    if (is_array($result)) {
        AppSystem::_alertMensagem("Aten��o", $result);
    } else {
        AppSystem::_confirmRedirect("Dados cadastrados com sucesso", "/blog_comentario.php");
    }
}
?>
<?php include_once './header.php'; ?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Gerenciando Coment�rio</h1>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php
                    if (empty($id)) {
                        echo "Adicionar Novo";
                    } else {
                        echo "Alterar Registro";
                    }
                    ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" name="form" method="post">
                                <div class="form-group">
                                    <label>Post</label>
                                    <select name="id_blog" class="form-control">
                                        <option value="">[Selecione um Post]</option>
                                        <?php for ($i = 0; $i < count($list); $i++) { ?>
                                        <option value="<?php echo $list[$i]["id"]?>" <?php if ($list[$i]["id"] == $objBlogComentario->getIdBlog()) { echo "selected='selected'"; }?>><?php echo $list[$i]["title"]?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Autor do Coment�rio</label>
                                    <input name="author" class="form-control" value="<?php echo AppSystem::textByHtml($objBlogComentario->getAuthor()); ?>" placeholder="Campo autor" />
                                </div>
                                <div class="form-group">
                                    <label>Coment�rio</label>
                                    <textarea name="comment" class="form-control" rows="10" maxlength="500" placeholder="Campo coment�rio"><?php echo AppSystem::textByHtml($objBlogComentario->getComment()); ?></textarea>
                                </div>
                                <input name="insert_date" type="hidden" value="<?php echo $objBlogComentario->getInsertDate(); ?>" />
                                <div class="form-group">
                                    <label>Status do Coment�rio</label>
                                    <label class="radio-inline">
                                        <input name="status" type="radio" name="optionsRadiosInline" id="optionsRadiosInline1" value="2" <?php if ($objBlogComentario->getStatus() == 2 || empty($objBlogComentario->getStatus())) { echo "checked='checked'"; } ?>/>Pendente
                                    </label>
                                    <label class="radio-inline">
                                        <input name="status" type="radio" name="optionsRadiosInline" id="optionsRadiosInline2" value="1" <?php if ($objBlogComentario->getStatus() == 1) { echo "checked='checked'"; } ?>>Liberado
                                    </label>
                                </div>
                                <?php if(!empty($id)) { ?>
                                <input name="id" type="hidden" value="<?php echo $objBlogComentario->getId(); ?>" />
                                <?php } ?>
                                <button type="submit" class="btn btn-default">Salvar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /#wrapper -->
<?php include_once './footer.php'; ?>