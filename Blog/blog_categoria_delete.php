<?php
require_once './config.php';

use application\BlogCategoria\BlogCategoria;
use application\lib\AppSystem;

// Requisitando id para excluir registro
$id = $_REQUEST["id"];

$objBlogCategoria = new BlogCategoria();
// Removendo Registro
$result = $objBlogCategoria->removeRegistro("blog_categoria", "id", $id);
// Redirecionando para Listagem
AppSystem::_redirect("/blog_categoria.php");

?>
